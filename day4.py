#!/usr/bin/env python

# https://adventofcode.com/2023/day/4

import re
from collections import defaultdict

from rich import print

input_filename = "input4.txt"
# input_filename = "sample4.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

part1 = 0
for line in data:
    c, line = line.split(':')
    winning, supplied = line.split('|')
    winning = set(map(int, re.split(r'\s+', winning.strip())))
    supplied = set(map(int, re.split(r'\s+', supplied.strip())))
    part1 += int(2 ** (len(winning & supplied)- 1))

print("part1:", part1)

iterations_for_card = defaultdict(lambda: 1)

for line in data:
    c, line = line.split(':')
    winning, supplied = line.split('|')
    winning = set(map(int, re.split(r'\s+', winning.strip())))
    supplied = set(map(int, re.split(r'\s+', supplied.strip())))
    match = re.search('Card\s+(\d+)', c)
    cn = int(match.group(1))
    wins = len(winning & supplied)

    iterations_for_card.setdefault(cn, 1)

    for i in range(wins):
        iterations_for_card[cn + i + 1] += iterations_for_card[cn]

print("part2:", sum(iterations_for_card.values()))
