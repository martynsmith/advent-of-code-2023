#!/usr/bin/env python

from functools import reduce
from rich import print
from itertools import cycle
from pathlib import Path
import math
import re

# https://adventofcode.com/2023/day/8

INPUT_FILENAME = "input8.txt"
# INPUT_FILENAME = "sample8.txt"

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().splitlines()]

paths = {}
direction_list = list(data[0])
p1_location = 'AAA'
p2_locations = []

for line in data[2:]:
    match = re.search(r'(\w+) = \((\w+), (\w+)', line)
    paths[match.group(1)] = dict(L=match.group(2), R=match.group(3))
    if match.group(1).endswith('A'):
        p2_locations.append(match.group(1))

part1 = 0
directions = cycle(direction_list)
while p1_location != 'ZZZ':
    p1_location = paths[p1_location][next(directions)]
    part1 += 1

print("part1:", part1)

def lcm(a, b):
    return abs(a * b) // math.gcd(a, b)

p2_steps = []

for location in p2_locations:
    steps = 0
    directions = cycle(direction_list)
    while not location.endswith('Z'):
        location = paths[location][next(directions)]
        steps += 1
    p2_steps.append(steps)

print("part2:", reduce(lcm, p2_steps))