#!/usr/bin/env python

from itertools import combinations
from pathlib import Path
from rich import print
from aoclib import Vector

# https://adventofcode.com/2023/day/11

INPUT_FILENAME = "input11.txt"
# INPUT_FILENAME = "sample11.txt"

PART1_GAP_WIDTH = 2
PART2_GAP_WIDTH = 1_000_000

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().splitlines()]

part1 = 0
part2 = 0

# Expand vertical lines
x_gaps = set()
for i, c in enumerate(data[0]):
    if set(l[i] for l in data) == {'.'}:
        x_gaps.add(i)

# Expand horizontal lines
y_gaps = set()
for i, line in enumerate(data):
    if set(line) == {'.'}:
        y_gaps.add(i)

galaxies = set()

for y, line in enumerate(data):
    for x, char in enumerate(line):
        if char == '#':
            galaxies.add(Vector(x, y))

part1 = 0
part2 = 0

for g1, g2 in combinations(galaxies, 2):
    min_x = min(g1.x, g2.x)
    max_x = max(g1.x, g2.x)
    min_y = min(g1.y, g2.y)
    max_y = max(g1.y, g2.y)

    for x in x_gaps:
        if min_x < x < max_x:
            # the -1 is because the regular math will count the line once
            part1 += PART1_GAP_WIDTH - 1
            part2 += PART2_GAP_WIDTH - 1

    for y in y_gaps:
        if min_y < y < max_y:
            # the -1 is because the regular math will count the line once
            part1 += PART1_GAP_WIDTH - 1
            part2 += PART2_GAP_WIDTH - 1

    part1 += (max_x - min_x) + (max_y - min_y)
    part2 += (max_x - min_x) + (max_y - min_y)

print("part1:", part1)
print("part2:", part2)
