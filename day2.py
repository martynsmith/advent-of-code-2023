#!/usr/bin/env python

# https://adventofcode.com/2023/day/2

input_filename = "input2.txt"
# input_filename = "sample2.txt"

import re
from operator import mul
from functools import reduce

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

rmax = 12
gmax = 13
bmax = 14

part1 = 0
part2 = 0

for line in data:
    id, data = line.split(':', maxsplit=1)
    games = data.split(';')

    id = int(re.sub(r'[^0-9]*', '', id))
    possible = True

    cmin = {}

    for game in games:
        cubes = dict(red=0, green=0, blue=0)
        for count, colour in re.findall(r'(\d+) (\w+)', game):
            count = int(count)
            cubes[colour] += count
        if cubes['red'] > rmax or cubes['green'] > gmax or cubes['blue'] > bmax:
            possible = False

        for colour, count in cubes.items():
            if count == 0:
                continue
            if colour in cmin:
                cmin[colour] = max(cubes[colour], cmin[colour])
            else:
                cmin[colour] = cubes[colour]

    part2 += reduce(mul, cmin.values())

    if possible:
        part1 += id

print("part1:", part1)
print("part2:", part2)
