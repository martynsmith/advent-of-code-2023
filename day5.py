#!/usr/bin/env python

# https://adventofcode.com/2023/day/5

import re
from rich import print
from typing import NamedTuple
from itertools import pairwise, chain


class Map(NamedTuple):
    dst_start: int
    src_start: int
    length: int

    @property
    def src_end(self) -> int:
        return self.src_start + self.length - 1


class SeedRange(NamedTuple):
    start: int
    end: int


input_filename = "input5.txt"
# input_filename = "sample5.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

src = None
dst = None
smap = []

part1 = data.pop(0)
part1 = list(map(int, part1.split(': ')[1].split()))
part2 = [SeedRange(s, s + l - 1) for s, l in list(pairwise(part1))[::2]]


def apply_map(seed: int, map: list[Map]) -> int:
    for m in map:
        if m.src_start <= seed < m.src_start + m.length:
            return seed + m.dst_start - m.src_start
    return seed


def apply_single(seed: SeedRange, map: Map) -> tuple[list[SeedRange], list[SeedRange]]:
    if seed.end < map.src_start or seed.start > map.src_end:
        return [seed], []

    unmapped: list[SeedRange] = []
    mapped: list[SeedRange] = []

    if seed.start < map.src_start:
        unmapped.append(SeedRange(seed.start, min(seed.end, map.src_start - 1)))

    mstart = max(seed.start, map.src_start)
    mend = min(seed.end, map.src_end)
    if mstart <= mend:
        mapped.append(SeedRange(
            mstart + map.dst_start - map.src_start, mend + map.dst_start - map.src_start
        ))

    if seed.end > map.src_end:
        unmapped.append(SeedRange(map.src_end + 1, seed.end))

    return unmapped, mapped


def apply_range(seed: SeedRange, map: list[Map]) -> list[SeedRange]:
    mapped = []
    unmapped = [seed]

    for m in map:
        new_unmapped = []
        for seed in unmapped:
            extra_unmapped, extra_mapped = apply_single(seed, m)
            new_unmapped.extend(extra_unmapped)
            mapped.extend(extra_mapped)
        unmapped = new_unmapped

    return mapped + unmapped


for line in data:
    if match := re.search(r'(\w+)-to-(\w+) map:', line):
        if src or dst:
            part1 = [apply_map(s, smap) for s in part1]
            part2 = list(chain.from_iterable([apply_range(s, smap) for s in part2]))
        smap = []
        src, dst = match.groups()
        continue

    if not line:
        continue

    smap.append(Map(*map(int, line.split())))

part1 = [apply_map(s, smap) for s in part1]
part2 = list(chain.from_iterable([apply_range(s, smap) for s in part2]))
print("part1:", min(part1))
print("part2:", min(seed.start for seed in part2))
