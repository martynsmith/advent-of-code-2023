#!/usr/bin/env python

from rich import print
from pathlib import Path

from aoclib import Vector, print_grid, get_bounds

# https://adventofcode.com/2023/day/16

INPUT_FILENAME = "input16.txt"
# INPUT_FILENAME = "sample16.txt"

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().splitlines()]

grid = {}

for y, line in enumerate(data):
    for x, char in enumerate(line):
        grid[Vector(x, y)] = char

LEFT = Vector(-1, 0)
RIGHT = Vector(1, 0)
UP = Vector(0, -1)
DOWN = Vector(0, 1)

class d:
    LEFT = LEFT
    RIGHT = RIGHT
    UP = UP
    DOWN = DOWN


def track_beam(pos: Vector = Vector(0, 0), direction: Vector = RIGHT, seen=None):
    if seen is None:
        seen = set()
    while (pos, direction) not in seen:
        if pos in grid:
            seen.add((pos, direction))

        match grid.get(pos):
            case None:
                return seen
            case '.':
                pos += direction
            case '/':
                match direction:
                    case d.LEFT:
                        direction = DOWN
                    case d.DOWN:
                        direction = LEFT
                    case d.RIGHT:
                        direction = UP
                    case d.UP:
                        direction = RIGHT
                pos += direction
            case '\\':
                match direction:
                    case d.LEFT:
                        direction = UP
                    case d.UP:
                        direction = LEFT
                    case d.RIGHT:
                        direction = DOWN
                    case d.DOWN:
                        direction = RIGHT
                pos += direction
            case '|':
                if direction in {LEFT, RIGHT}:
                    return seen | track_beam(pos + UP, UP, seen) | track_beam(pos + DOWN, DOWN, seen)
                if direction in {UP, DOWN}:
                    pos += direction
            case '-':
                if direction in {UP, DOWN}:
                    return seen | track_beam(pos + LEFT, LEFT, seen) | track_beam(pos + RIGHT, RIGHT, seen)
                if direction in {LEFT, RIGHT}:
                    pos += direction
            case _:
                raise Exception(f"{grid.get(pos)=}")

    return seen

def vec_count(seen) -> int:
    return len({p for p, _ in seen})


print("part1:", vec_count(track_beam()))

print("you're going to have to wait about a minute :-(")
tl, br = get_bounds(grid)

part2 = 0

for x in range(tl.x, br.x+1):
    part2 = max(
        part2,
        vec_count(track_beam(Vector(x, 0), DOWN)),
        vec_count(track_beam(Vector(x, br.y), UP)),
    )

for y in range(tl.y, br.y+1):
    part2 = max(
        part2,
        vec_count(track_beam(Vector(0, y), RIGHT)),
        vec_count(track_beam(Vector(br.x, y), LEFT)),
    )

print("part2:", part2)
