#!/usr/bin/env python

from rich import print
from pathlib import Path

from aoclib import Vector, get_bounds

# https://adventofcode.com/2023/day/13

INPUT_FILENAME = "input13.txt"
# INPUT_FILENAME = "sample13.txt"

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().split('\n\n')]

part1 = 0
part2 = 0


def is_mirror_x(grid, br, x):
    lx = x
    rx = x + 1

    while lx >= 0 and rx <= br.x:
        if not all(grid.get(Vector(lx, y)) == grid.get(Vector(rx, y)) for y in range(br.y + 1)):
            return False
        lx -= 1
        rx += 1

    return True


def is_mirror_y(grid, br, y):
    ly = y
    ry = y + 1

    while ly >= 0 and ry <= br.y:
        if not all(grid.get(Vector(x, ly)) == grid.get(Vector(x, ry)) for x in range(br.x + 1)):
            return False
        ly -= 1
        ry += 1

    return True


def get_value(grid, exclude=None):
    tl, br = get_bounds(grid)

    for x in range(br.x):
        if exclude == ('x', x):
            continue
        if is_mirror_x(grid, br, x):
            return ('x', x, x + 1)

    for y in range(br.y):
        if exclude == ('y', y):
            continue
        if is_mirror_y(grid, br, y):
            return ('y', y, (y + 1) * 100)

    return 0, 0, 0


for pattern in data:
    grid = {}
    for y, line in enumerate(pattern.splitlines()):
        for x, char in enumerate(line):
            grid[Vector(x, y)] = char

    axis, index, p1_value = get_value(grid)
    part1 += p1_value

    for v in list(grid.keys()):
        orig = grid[v]
        grid[v] = '.' if orig == '#'  else '#'
        _, _, p2_value = get_value(grid, exclude=(axis, index))
        grid[v] = orig
        if p2_value and p2_value != p1_value:
            break

    part2 += p2_value


print("part1:", part1)
print("part2:", part2)
