#!/usr/bin/env python

from functools import reduce
from rich import print
from itertools import cycle
from pathlib import Path
import math
import re

from aoclib import Vector, print_grid, get_bounds

# https://adventofcode.com/2023/day/14

INPUT_FILENAME = "input14.txt"
# INPUT_FILENAME = "sample14.txt"

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().splitlines()]

part1 = 0
part2 = 0

grid = {}

for y, line in enumerate(data):
    for x, char in enumerate(line):
        grid[Vector(x, y)] = char

tl, br = get_bounds(grid)

NORTH = Vector(0, -1)
WEST = Vector(-1, 0)
SOUTH = Vector(0, 1)
EAST = Vector(1, 0)

CYCLE = [NORTH, WEST, SOUTH, EAST]

def tilt(grid, direction):
    # So long as we iterate in the correct direction, we can be confident that we move the direction-most
    # mirrors before any others meaning we can compute the tilt in a single pass.
    if direction in {NORTH, WEST}:
        # We read the vectors in with both X and Y incrementing as we added them, so NORTH and WEST are
        # both directions where the "natural" order is what we want
        q = grid.items()
    else:
        # We want to reverse the "natural" order here
        q = reversed(grid.items())

    for v, char in q:
        if char != 'O':
            continue

        tilted_position = v
        potential_position = tilted_position + direction
        while grid.get(potential_position) == '.':
            tilted_position = potential_position
            potential_position = tilted_position + direction

        if tilted_position != v:
            grid[v], grid[tilted_position] = grid[tilted_position], grid[v]

p1grid = grid.copy()
tilt(p1grid, NORTH)

def get_load(grid):
    load = 0
    for v, char in grid.items():
        if char != 'O':
            continue
        load += br.y - v.y + 1
    return load

print("part1:", get_load(p1grid))

def get_key(grid):
    return tuple(sorted(v for v, c in grid.items() if c == 'O'))

seen = {get_key(grid): 0}
load_for_iter = {}
CYCLE_COUNT = 1_000_000_000
i = 0
while i < CYCLE_COUNT:
    for direction in CYCLE:
        tilt(grid, direction)
    load_for_iter[i] = get_load(grid)
    # print(i, load_for_iter[i])

    key = get_key(grid)
    if key in seen:
        cycle_start = seen[key]
        cycle_end = i

        answer_index = cycle_start + (CYCLE_COUNT - cycle_start - 1) % (cycle_end - cycle_start)
        print("part2:", load_for_iter[answer_index])
        break
    seen[key] = i
    i += 1
