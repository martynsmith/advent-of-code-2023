#!/usr/bin/env python

from functools import reduce
from rich import print
from itertools import cycle, combinations
from pathlib import Path
import math
import re

from aoclib import Vector, print_grid, SQUARE_NEIGHBOURS, get_bounds

# https://adventofcode.com/2023/day/10

INPUT_FILENAME = "input10.txt"
# INPUT_FILENAME = "sample10.txt"

# data = Path(INPUT_FILENAME).read_text()
text = (
    Path(INPUT_FILENAME).read_text()
    .replace('|', '│')
    .replace('-', '─')
    .replace('L', '└')
    .replace('J', '┘')
    .replace('7', '┐')
    .replace('F', '┌')
)
data = [line.strip() for line in text.splitlines()]

LEFT = Vector(-1, 0) * 2
RIGHT = Vector(1, 0) * 2
UP = Vector(0, -1) * 2
DOWN = Vector(0, 1) * 2

DIRECTIONS = {LEFT, RIGHT, UP, DOWN}

PIPE = {
    '│': {UP, DOWN},
    '─': {LEFT, RIGHT},
    '└': {UP, RIGHT},
    '┘': {UP, LEFT},
    '┐': {LEFT, DOWN},
    '┌': {DOWN, RIGHT},
}


grid = {}
start = None

for y, line in enumerate(data):
    for x, char in enumerate(line):
        grid[Vector(x * 2, y * 2)] = char
        if char == 'S':
            assert start is None
            start = Vector(x * 2, y * 2)

steps = {start: 0}
current = {start}
step = 0

while current:
    step += 1
    next_current = set()
    for pos in current:
        for d in SQUARE_NEIGHBOURS:
            d *= 2
            n = pos + d
            if n in steps:
                continue
            if grid[pos] != 'S' and d not in PIPE[grid[pos]]:
                continue
            pipe = PIPE.get(grid.get(n))
            if not pipe:
                continue
            if -d not in pipe:
                continue

            steps[n] = step
            next_current.add(n)
            steps[pos + d // 2] = 0
            if d.x:
                grid[pos + d // 2] = '─'
            else:
                grid[pos + d // 2] = '│'

    assert len(next_current) <= 2
    if len(next_current) == 1:
        for v1, v2 in combinations(current | next_current, 2):
            if v1 - v2 in DIRECTIONS:
                v = v1 - (v1 - v2) // 2
                grid[v] = '─'
                steps[v] = 0
    current = next_current

print("part1:", max(steps.values()))

tl, br = get_bounds(grid.keys())

for y in range(tl.y, br.y + 1):
    for x in range(tl.x, br.x + 1):
        v = Vector(x, y)
        if v not in grid:
            grid[v] = ' '

to_process = list(set(
    [tl + Vector(x, 0) for x in range(br.x + 1)]
    + [br - Vector(x, 0) for x in range(br.x + 1)]
    + [tl + Vector(0, y) for y in range(br.y + 1)]
    + [br - Vector(0, y) for y in range(br.y + 1)]
))
to_process = [v for v in to_process if v not in steps]
seen = set(steps.keys())

for v in to_process:
    grid[v] = 'O'

while to_process:
    v = to_process.pop()
    seen.add(v)
    # if grid[v] == '.':
    grid[v] = 'O'
    for sv in v.square_neighbours():
        if sv in grid and sv not in seen:
            to_process.append(sv)

for v, c in list(grid.items()):
    if c in ('*', 'O', ' '):
        del grid[v]

for v, c in list(grid.items()):
    if v in steps:
        grid.pop(v, None)


print("part2:", len(grid))
