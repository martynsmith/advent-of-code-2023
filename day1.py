#!/usr/bin/env python
import re

# https://adventofcode.com/2023/day/1

input_filename = "input1.txt"
# input_filename = "sample1.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

part1 = 0
part2 = 0

digits = {
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
}

pattern = '|'.join(digits.keys())

for line in data:
    first = None
    last = None

    p1_working = re.sub(r'[^0-9]', '', line)
    part1 += int(p1_working[0] + p1_working[-1])

    for index, char in enumerate(line):
        if char in [str(i) for i in range(0, 10)]:
            if first is None:
                first = char
            last = char
        for word, number in digits.items():
            if line[index:].startswith(word):
                if first is None:
                    first = number
                last = number

    n = int(first + last)
    part2 += n

print("part1", part1)
print("part2", part2)
