#!/usr/bin/env python

from rich import print

# https://adventofcode.com/2023/day/7

input_filename = "input7.txt"
# input_filename = "sample7.txt"

cards = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2']

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

hands = []

for line in data:
    hand, bid = line.split()

    hand = list(hand)
    card_counts = {}
    for card in hand:
        card_counts.setdefault(card, 0)
        card_counts[card] += 1

    card_counts = sorted(card_counts.values())

    if card_counts == [5]:
        hand_strength = 7
    elif card_counts == [1, 4]:
        hand_strength = 6
    elif card_counts == [2, 3]:
        hand_strength = 5
    elif card_counts == [1, 1, 3]:
        hand_strength = 4
    elif card_counts == [1, 2, 2]:
        hand_strength = 3
    elif card_counts == [1, 1, 1, 2]:
        hand_strength = 2
    else:
        hand_strength = 1

    card_strength = [len(cards) - cards.index(c) for c in hand]
    hands.append((hand_strength, *card_strength, "".join(hand), int(bid)))

part1 = 0

for rank, hand in enumerate(sorted(hands)):
    rank += 1
    bid = hand[-1]
    part1 += rank * bid

print("part1:", part1)

## PART 2

hands = []
cards = ['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J']

def get_hand_strength(hand: list[int]) -> int:
    if 'J' in hand:
        sub_strengths = []
        i = hand.index('J')
        for card in cards:
            if card == 'J':
                continue
            hand[i] = card
            sub_strengths.append(get_hand_strength(hand))
        hand[i] = 'J'
        return max(sub_strengths)

    card_counts = {}
    for card in hand:
        card_counts.setdefault(card, 0)
        card_counts[card] += 1

    card_counts = sorted(card_counts.values())

    if card_counts == [5]:
        hand_strength = 7
    elif card_counts == [1, 4]:
        hand_strength = 6
    elif card_counts == [2, 3]:
        hand_strength = 5
    elif card_counts == [1, 1, 3]:
        hand_strength = 4
    elif card_counts == [1, 2, 2]:
        hand_strength = 3
    elif card_counts == [1, 1, 1, 2]:
        hand_strength = 2
    else:
        hand_strength = 1

    return hand_strength


for line in data:
    hand, bid = line.split()

    hand = list(hand)
    card_strength = [len(cards) - cards.index(c) for c in hand]
    hand_strength = get_hand_strength(hand)
    hands.append((hand_strength, *card_strength, "".join(hand), int(bid)))

part2 = 0

for rank, hand in enumerate(sorted(hands)):
    rank += 1
    bid = hand[-1]
    part2 += rank * bid

print("part2:", part2)
