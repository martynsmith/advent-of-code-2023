#!/usr/bin/env python

from rich import print
from pathlib import Path
import re

# https://adventofcode.com/2023/day/15

INPUT_FILENAME = "input15.txt"
# INPUT_FILENAME = "sample15.txt"

data = Path(INPUT_FILENAME).read_text().strip()

part1 = 0
part2 = 0


def get_hash(word):
    current = 0
    for char in word:
        current += ord(char)
        current *= 17
        current %= 256
    return current


boxes = {box: [] for box in range(256)}
for word in data.split(','):
    part1 += get_hash(word)

    if match := re.search('(.*)=(\d+)', word):
        label, focal_length = match.groups()
        focal_length = int(focal_length)
    elif match := re.search('(.*)-', word):
        label, = match.groups()
        focal_length = None

    box = boxes[get_hash(label)]
    index = next((index for index, item in enumerate(box) if item[0] == label), None)

    if focal_length is None:
        if index is not None:
            del box[index]
    elif index is not None:
        box[index] = (label, focal_length)
    else:
        box.append((label, focal_length))

for box_number, box in boxes.items():
    for index, (label, focal_length) in enumerate(box):
        power = (box_number + 1) * (index + 1) * focal_length
        part2 += power

print("part1:", part1)
print("part2:", part2)
