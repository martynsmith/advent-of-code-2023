#!/usr/bin/env python

import re

# https://adventofcode.com/2023/day/6

input_filename = "input6.txt"
# input_filename = "sample6.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

times = map(int, data[0].strip().split(':')[1].split())
distances = map(int, data[1].strip().split(':')[1].split())
races = zip(times, distances)


def get_distances(time) -> list[int]:
    distances = []

    for t in range(time + 1):
        distances.append(t * (time - t))

    return distances


part1 = 1
for time, distance in races:
    part1 *= len([d for d in get_distances(time) if d > distance])

print("part1:", part1)

time = int(re.sub(r'\D', '', data[0]))
distance = int(re.sub(r'\D', '', data[1]))

first_low = 0
first_high = time // 2

while (first_high - first_low > 1):
    first = first_low + (first_high - first_low) // 2
    if first * (time - first) < distance:
        first_low = first
    else:
        first_high = first

while first * (time - first) < distance:
    first += 1


last_low = time // 2
last_high = time

while (last_high - last_low > 1):
    last = last_low + (last_high - last_low) // 2
    if last * (time - last) < distance:
        last_high = last
    else:
        last_low = last

while last * (time - last) < distance:
    last -= 1


print("part2:", last - first + 1)
