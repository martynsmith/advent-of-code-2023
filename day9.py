#!/usr/bin/env python

from rich import print

# https://adventofcode.com/2023/day/9

input_filename = "input9.txt"
# input_filename = "sample9.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]


def deltas(vals: list[int]):
    return [vals[i + 1] - v for i, v in enumerate(vals[:-1])]


def p1calc(vals: list[int]):
    if set(vals) == {0}:
        return vals

    nvals = p1calc(deltas(vals))
    return vals + [vals[-1] + nvals[-1]]


def p2calc(vals: list[int]):
    if set(vals) == {0}:
        return vals

    nvals = p2calc(deltas(vals))
    return [vals[0] - nvals[0]] + vals


part1 = 0
part2 = 0

for line in data:
    values = list(map(int, line.split()))
    part1 += p1calc(values)[-1]
    part2 += p2calc(values)[0]

print("part1:", part1)
print("part2:", part2)
