#!/usr/bin/env python
from functools import cache

from rich import print
from pathlib import Path

# https://adventofcode.com/2023/day/12

INPUT_FILENAME = "input12.txt"
# INPUT_FILENAME = "sample12.txt"

# data = Path(INPUT_FILENAME).read_text()
data = [line.strip() for line in Path(INPUT_FILENAME).read_text().splitlines()]


@cache
def find_matches(springs: str, count: int):
    springs += "$"
    index = 0
    results = []
    while "#" not in springs[:index] and index < len(springs):
        subset = springs[index:index + count]
        if len(subset) == count and set(subset).issubset({'?', '#'}) and set(springs[index + count]).issubset(
            {'?', '.', '$'}):
            results.append(index)
        index += 1
    return results


@cache
def count_combos(springs, counts) -> int:
    result = 0
    if not springs.replace('.', '').replace('?', '') and not counts:
        return 1

    if not springs or not counts:
        return 0

    for start in find_matches(springs, counts[0]):
        subsprings = springs[start + counts[0] + 1:]
        result += count_combos(subsprings, counts[1:])

    return result


part1 = 0
part2 = 0

for line in data:
    springs, counts = line.split(' ')
    counts = tuple(map(int, counts.split(',')))
    part1 += count_combos(springs, counts)

    springs = "?".join([springs] * 5)
    counts *= 5
    part2 += count_combos(springs, counts)

print("part1:", part1)
print("part2:", part2)
