#!/usr/bin/env python

# https://adventofcode.com/2023/day/3

from aoclib import Vector, print_grid

input_filename = "input3.txt"
# input_filename = "sample3.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

grid = {}

for y, line in enumerate(data):
    for x, char in enumerate(line):
        grid[Vector(x, y)] = char

symbols = {'*', '#', '+', '$', '@', '%', '-', '=', '/', '&'}
digits = {str(d) for d in range(10)}
LEFT = Vector(-1, 0)
RIGHT = Vector(1, 0)

seen = set()

part1 = 0

def get_number(v) -> tuple[int, set[Vector]]:
    seen = set()
    char = grid[v]

    while grid.get(v + LEFT) in digits:
        v += LEFT
        char = grid[v]
    number = char
    seen.add(v)
    while grid.get(v + RIGHT) in digits:
        v += RIGHT
        seen.add(v)
        char = grid[v]
        number += char

    return int(number), seen

for v, char in grid.items():
    if v in seen:
        continue
    if char in digits:
        include = False
        for pv in v.diagonal_neighbours():
            if grid.get(pv) in symbols:
                include = True

        if include:
            number, tseen = get_number(v)
            seen |= tseen
            part1 += number

print("part1:", part1)

part2 = 0
for v, char in grid.items():
    if char != '*':
        continue

    n1 = None
    n2 = None
    for pv in v.diagonal_neighbours():
        if grid.get(pv) in digits:
            if not n1:
                n1, n1v = get_number(pv)
            elif pv not in n1v:
                n2, _ = get_number(pv)

    if n1 and n2:
        part2 += n1 * n2

print("part2:", part2)
